import { expect, tap } from '@pushrocks/tapbundle';
import * as smartupdate from '../ts/index.js';

let testSmartUpdate: smartupdate.SmartUpdate;

tap.test('should create an instance of SmartUpdate', async () => {
  testSmartUpdate = new smartupdate.SmartUpdate();
});

tap.test('should check for a npm module', async () => {
  const result = await testSmartUpdate.check('lodash', '1.0.5');
  expect(result).toBeTrue();
});

tap.start();
