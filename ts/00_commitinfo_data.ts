/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartupdate',
  version: '2.0.4',
  description: 'update your tools in a smart way'
}
